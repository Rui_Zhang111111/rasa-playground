# This files contains your custom actions which can be used to run
# custom Python code.
#
# See this guide on how to implement these action:
# https://rasa.com/docs/rasa/custom-actions



from typing import Any, Text, Dict, List

from rasa_sdk import Action, Tracker
from rasa_sdk.events import SlotSet, EventType
from rasa_sdk.executor import CollectingDispatcher
import webbrowser
from rasa_sdk.events import Restarted
from weather import Weather



class UserForm(Action):
    def name(self) -> Text:
        return "user_details_form"

    def run(
        self, dispatcher: CollectingDispatcher, tracker: Tracker, domain: Dict
    ) -> List[EventType]:
        required_slots = ["name", "age"]

        for slot_name in required_slots:
            if tracker.slots.get(slot_name) is None:
                # The slot is not filled yet. Request the user to fill this slot next.
                return [SlotSet("requested_slot", slot_name)]

        # All slots are filled.
        return [SlotSet("requested_slot", None)]

class ActionSubmit(Action):
    def name(self) -> Text:
        return "action_submit"

    def run(
        self,
        dispatcher,
        tracker: Tracker,
        domain: "DomainDict",
    ) -> List[Dict[Text, Any]]:
        dispatcher.utter_message(template="utter_details_thanks",
                                 Name=tracker.get_slot("name"),
                                 age=tracker.get_slot("age"))

        return[]


class ActionlinkStudy(Action):

    def name(self) -> Text:
        return "action_study"

    def run(self, dispatcher: CollectingDispatcher,
            tracker: Tracker,
            domain: Dict[Text, Any]) -> List[Dict[Text, Any]]:


       
        dispatcher.utter_message(text='[Some useful carrer paths](https://www2.deloitte.com/us/en/insights/deloitte-review/issue-21/changing-nature-of-careers-in-21st-century.html)')

        return []


class ActionlinkMovie(Action):

    def name(self) -> Text:
        return "action_movie"

    def run(self, dispatcher: CollectingDispatcher,
            tracker: Tracker,
            domain: Dict[Text, Any]) -> List[Dict[Text, Any]]:


       
        dispatcher.utter_message(text='[Best Movies of all time](https://www.imdb.com/chart/top/)')

        return []



class ActionYoutubeVideos(Action):

    def name(self) -> Text:
        return "action_play_video"

    def run(self, dispatcher: CollectingDispatcher,
            tracker: Tracker,
            domain: Dict[Text, Any]) -> List[Dict[Text, Any]]:

        #dispatcher.utter_message(text="Hello World!")
        dispatcher.utter_message(attachment={
            "type": "video",
            "payload": {
                "title": "This video will help you to relax, just close your eyes and put on your headphones :) ",
                "src": "https://www.youtube.com/embed/9BD1y0TOk3o"
            }
        })

        return []
        

class Actionexercise(Action):
     
    def name(self) -> Text:
        return "action_exercise"

    def run(self,
        dispatcher: CollectingDispatcher,
        tracker: Tracker,
        domain: Dict[Text, Any],
    ) -> List[EventType]:
        dispatcher.utter_message(attachment={
            "type": "video",
            "payload": {
                "title": "let's try some yoga :) ",
                "src": "https://www.youtube.com/embed/EwQkfoKxRvo"
            }
        })
   
        dispatcher.utter_message(attachment={
            "type": "video",
            "payload": {
                "title": "let's try some workout :) ",
                "src": "https://www.youtube.com/embed/24fdcMw0Bj0"
      
             }
        })
       

        return []

class Actionweather(Action):

    def name(self) -> Text:
        return "action_weather"

    def run(self, dispatcher: CollectingDispatcher,
            tracker: Tracker,
            domain: Dict[Text, Any]) -> List[Dict[Text, Any]]:
        city=tracker.latest_message['text']
        temp=int(Weather(city)['temp']-273)
        dispatcher.utter_template("utter_temp",tracker,temp=temp)

        return []


